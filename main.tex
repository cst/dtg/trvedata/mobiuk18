\documentclass[twocolumn,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper,portrait,margin=2cm]{geometry}
\usepackage[british]{babel}
\usepackage[urw-garamond]{mathdesign}
\usepackage[T1]{fontenc}
\usepackage[hyphens]{url}
\usepackage[hidelinks]{hyperref}
\usepackage{doi}
\usepackage[numbers,sort]{natbib}
\urlstyle{rm}

\begin{document}
\frenchspacing\sloppy\pagestyle{empty}
\title{\vspace{-2em}Automerge: Real-time data sync between edge devices}
\author{Martin Kleppmann \hspace{3em} Alastair R.\ Beresford\\[6pt]
Department of Computer Science and Technology, University of Cambridge, UK}
\date{}
\maketitle
\begin{abstract}
We introduce Automerge, a JavaScript library for data synchronisation between mobile devices such as laptop computers and smartphones.
It allows users to read and modify data even while their device is offline, and it automatically merges changes made concurrently on different devices.
Unlike most existing data synchronisation systems, Automerge does not require data to be sent via a centralised server, but rather allows local and peer-to-peer networks to be used.
\end{abstract}

\section*{Background}
Many mobile apps require data to be synchronised (or \emph{replicated}) between several devices owned by the same user: for example, contacts, calendars, reminders, and passwords.
Moreover, there are many collaborative cloud-based apps, such as Google Docs (text documents and spreadsheets), Evernote (notes and ideas), and various business collaboration tools (such as Slack or Trello), which users may access from mobile devices.

Currently, these kinds of application are typically implemented by storing the primary copy of the data on a server.
Some applications allow the user to access and modify this data while offline, by storing a local copy of the data on the user's device, while other applications only work while online, because they require synchronous interaction with the server.

Allowing users to read and write a local copy of the data gives rise to performance and availability benefits, since the users do not need to wait for network communication when interacting with the application.
However, a challenge with this architecture is that potentially conflicting changes can be made concurrently on different devices that have copies of the data.
These changes need to be merged while resolving any conflicts that may have occurred.

Existing collaborative applications such as Google Docs use a technique called \emph{Operational Transformation} to perform this conflict resolution~\cite{Nichols:1995fd,DayRichter:2010tt}.
This technique has the downside that all changes to the data must be sent via a central server, potentially located on another continent, even if the collaborating devices are in the same room.
The server must be trusted to correctly process document edits, ruling out the use of end-to-end encryption.

\section*{Introducing Automerge}

Automerge is a data synchronisation and conflict resolution library that is compatible with any network topology.
It allows servers to be used optionally, but it also enables mobile devices to exchange data directly via a local radio link such as Bluetooth, via a local network, or via peer-to-peer networks.
To improve user privacy, it enables the use of end-to-end encryption, which ensures that servers never need to handle unencrypted data, and which cryptographically ensures the integrity of data exchanged between devices~\cite{Kleppmann:2018sp}.

Automerge provides a JSON data model~\cite{Kleppmann:2016ve} implemented as a \emph{Conflict-free Replicated Data Type} or \emph{CRDT}~\cite{Shapiro:2011un}.
This approach allows arbitrary concurrent changes to a shared dataset or document to be merged automatically in a principled manner.
We have formally verified that these algorithms provide \emph{strong eventual consistency}~\cite{Gomes:2017gy}.

To demonstrate the generality of Automerge, we have implemented several proof-of-concept sample applications: a project management application similar to Trello, a collaborative graphics editor~\cite{vanHardenberg:2018nw}, a note-taking tool, and more.
Thanks to Automerge, these applications support real-time collaboration: if users are online, edits made by one user are seen by their collaborators in a fraction of a second.
On the other hand, these applications are also fully functional while offline, and they synchronise their state with instances of the application on other devices when a network connection is available.

\bibliographystyle{plainnat}
\footnotesize
\bibliography{references}
\end{document}
